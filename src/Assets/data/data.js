const accounts = [
    {
        id : parseInt('001'),
        name : "Chelsea Football Club",
        nickName : "Chelsea FC",
        logo : "/chelsea.png",
        profile : {
            motto : "‘Nisi Dominus Frustra’",
            headerImg : "/hChelsea.jpg",
            profileImg : "/ppChelsea.jpg",
        },
        information : {
            nickName : "The Blues",
            founded : "10 March 1905",
            ground : "Stamford Bridge",
            capacity : 40834,
            owner : "Roman Abramovich",
            chairman : "Bruce Buck",
            headCoach : "Thomas Tuchel",
            league : "Premier League",
            website : "https://www.chelseafc.com/en"
        },
        achievements: [
            {
                id: parseInt('001'),
                image: "/pl.png",
                name: "Premier League",
                times: "5x"
            },
            {
                id: parseInt('002'),
                image: "/fa.png",
                name: "FA Cup",
                times: "8x"
            },
            {
                id: parseInt('002'),
                image: "/efl.png",
                name: "EFL Cup",
                times: "5x"
            },
            {
                id: parseInt('002'),
                image: "/cs.png",
                name: "Community Shield",
                times: "4x"
            },
            {
                id: parseInt('002'),
                image: "/ucl.png",
                name: "UCL",
                times: "2x"
            },
            {
                id: parseInt('002'),
                image: "/uel.png",
                name: "UEL",
                times: "2x"
            },
            {
                id: parseInt('002'),
                image: "/sc.png",
                name: "Super Cup",
                times: "2x"
            },
            {
                id: parseInt('002'),
                image: "/ucl.png",
                name: "Club World Cup",
                times: "1x"
            }
        ],
        posts: [
            {
                post_id : parseInt('001'),
                category: "season-review",
                title : `Season Review 2016/2017`,
                desc : `"The 2016–17 season was Chelsea's 103rd competitive season, 28th consecutive season in the top flight of English football."`,
                author : "Anto Kewer",
                thumbnail : "/sr1617.jpg",
                content : "Ini Content"
            },            {
                post_id : parseInt('002'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/sr2021.jpg",
                content : `Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021). Bermain tandang, The Blues hanya menang tipis 1-0.
                Tentu kemenangan tetaplah kemenangan. Pasukan Thomas Tuchel tetap membungkus 3 yang penting untuk menjaga persaingan di papan atas klasemen sementara.
               Kendati demikian, Brentford adalah salah satu tim promosi musim ini. Chelsea diharapkan bisa menang mudah dengan skor yang lebih besar.
               Lantas, apa kata Tuchel soal kesulitan Chelsea lawan Brentford kemarin?`
            },
            {
                post_id : parseInt('015'),
                category: "intro",
                title : `Chelsea Football Club Introduction`,
                desc : `"The 2016–17 season was Chelsea's 103rd competitive season, 28th consecutive season in the top flight of English football."`,
                author : "Anto Kewer",
                thumbnail : "/intro.jpg",
                content : "Ini Content"
            },
            {
                post_id : parseInt('016'),
                category: "news",
                title : `Conor Gallagher receives first England call-up`,
                desc : `"Chelsea's Conor Gallagher has scored four goals in 10 Premier League games on loan at Crystal Palace this season"`,
                author : "Anto Kewer",
                thumbnail : "/cg.jpg",
                content : "Ini Content"
            }
        ],
        following: false
    },
    {
        id : parseInt('002'),
        name : "Manchester United Football Club",
        nickName : "Man Utd",
        logo : "/mu.png",
        profile : {
            motto : "‘Concilio et Labore’",
            headerImg : "/hMu.jpg",
            profileImg : "/ppMu.jpg",
        },
        information : {
            nickName : "The Red Devils",
            founded : "1878",
            ground : "Old Trafford",
            capacity : 	74140,
            owner : "Manchester United plc",
            chairman : "Joel and Avram Glazer",
            headCoach : "Ole Gunnar Solskjær",
            league : "Premier League",
            website : "https://www.manutd.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('003'),
                category: "season-review",
                title : "Manchester Derby",
                desc : `“Man City have clear strategy but United have Cristiano Ronaldo as their derby difference-maker. United’s stellar attack keeps finding a way, but will it be enough to overcome Pep’s gameplan?”`,
                author : "Anto Kewer",
                thumbnail : "/cr.jpg",
                content : "Ini Content"
            },            {
                post_id : parseInt('004'),
                category: "season-review",
                title : "First footballer to earn more than $1 billion",
                desc : `“The Juventus player ranked fourth on the publication's 2020 Celebrity 100 list after earning $105 million (€92.9m) before taxes and fees in the past year. He beat arch-rival Lionel Messi by one spot with the Argentinian netting $104 million (€92.1m).”`,
                author : "Anto Kewer",
                thumbnail : "/cr2.webp",
                content : "Ini Content"
            }
        ],
        following: false
    },
    {
        id : parseInt('003'),
        name : "The Arsenal Football Club",
        nickName : "Arsenal",
        logo : "/arsenal.png",
        profile : {
            motto : "‘Victoria Concordia Crescit’",
            headerImg : "/hArsenal.jpg",
            profileImg : "/ppArsenal.jpg",
        },
        information : {
            nickName : "The Gunners",
            founded : "October 1886",
            ground : "Emirates Stadium",
            capacity : 60704,
            owner : "Kroenke Sports & Entertainment",
            chairman : "-",
            headCoach : "Mikel Arteta",
            league : "Premier League",
            website : "https://www.arsenal.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('005'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“The 2020–21 season was Arsenal's 29th season in the Premier League, their 101st consecutive season in the top flight of English football and their 104th season in the top flight overall. In addition to the domestic league, Arsenal participated in the FA Cup and participated in the EFL Cup. They also qualified for the UEFA Europa League for the fourth consecutive year. Arsenal kicked off the season by defeating league champions Liverpool in the FA Community Shield. ”`,
                author : "Anto Kewer",
                thumbnail : "/arse2122.jpg",
                content : "Ini Content"
            },            {
                post_id : parseInt('006'),
                category: "season-review",
                title : "Arsenal Midfielder Called Up To England Squad",
                desc : `“Emile Smith Rowe has been called up to the England senior team for the first time in his career after Marcus Rashford, James Ward-Prowse, Mason Mount and Luke Shaw withdraw from Gareth Southgate's squad. The Arsenal midfielder has been in a rich vein of form and will now get the chance to impress Southgate ahead of England's final World Cup 2022 qualifers against Albania and San Marino.”`,
                author : "Anto Kewer",
                thumbnail : "/arsenal2.jpeg",
                content : "Ini Content"
            }
        ],
        following: false
    },
    {
        id : parseInt('004'),
        name : "Liverpool Football Club",
        nickName : "Liverpool",
        logo : "/liverpool.png",
        profile : {
            motto : "‘God hath granted us this ease’",
            headerImg : "/hLiv.jpg",
            profileImg : "/ppLiv.jpeg",
        },
        information : {
            nickName : "The Reds",
            founded : "3 June 1892",
            ground : "Anfield",
            capacity : 	53394,
            owner : "Fenway Sports Group",
            chairman : "Tom Werner",
            headCoach : "Jürgen Klopp",
            league : "Premier League",
            website : "https://www.liverpoolfc.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('007'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/pool.jpeg",
                content : "Ini Content"
            },            {
                post_id : parseInt('002'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/pool2.webp",
                content : "Ini Content"
            }
        ],
        following: false 
    },
    {
        id : parseInt('005'),
        name : "Manchester City Football Club",
        nickName : "Man City",
        logo : "/city.png",
        profile : {
            motto : "‘Superbia in proelia’",
            headerImg : "/hCity.jpg",
            profileImg : "/ppCity.jpg",
        },
        information : {
            nickName : "The Citizens",
            founded : "16 April 1894",
            ground : "City of Manchester Stadium",
            capacity : 	53400,
            owner : "City Football Group",
            chairman : "Khaldoon Al Mubarak",
            headCoach : "Pep Guardiola",
            league : "Premier League",
            website : "http://www.mancity.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('008'),
                category: "season-review",
                title : "Gundogan says Man Utd got off lightly ",
                desc : `“Manchester City midfielder Ilkay Gundogan believes rivals Manchester United got lucky in Saturday’s derby as his side could have been “3-0 or 4-0” ahead at half-time, such was their dominance. ”`,
                author : "Anto Kewer",
                thumbnail : "/mc.jpeg",
                content : "Ini Content"
            },            {
                post_id : parseInt('002'),
                category: "season-review",
                title : "Man Utd 0-2 Man City: Champions show quality",
                desc : `“After the nightmare of that 5-0 Liverpool defeat nearly a fortnight ago, it was looking ominous for United as City took the lead on seven minutes through Eric Bailly's own goal as he stretched to intercept Joao Cancelo's dangerous cross. ”`,
                author : "Anto Kewer",
                thumbnail : "/mc21.webp",
                content : "Ini Content"
            }
        ],
        following: false
    },
    {
        id : parseInt('006'),
        name : "Ajax",
        nickName : "Ajax",
        logo : "/ajax.png",
        profile : {
            motto : "‘Superbia in proelia’",
            headerImg : "/hCity.jpg",
            profileImg : "/ppCity.jpg",
        },
        information : {
            nickName : "The Citizens",
            founded : "16 April 1894",
            ground : "City of Manchester Stadium",
            capacity : 	53400,
            owner : "City Football Group",
            chairman : "Khaldoon Al Mubarak",
            headCoach : "Pep Guardiola",
            league : "Premier League",
            website : "http://www.mancity.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('009'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/ajax.jpeg",
                content : "Ini Content"
            },            {
                post_id : parseInt('010'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/ajax2.jpg",
                content : "Ini Content"
            }
        ],
        following: false
    },
    {
        id : parseInt('007'),
        name : "Barcelona",
        nickName : "Barcelona",
        logo : "/barca.png",
        profile : {
            motto : "‘Superbia in proelia’",
            headerImg : "/hCity.jpg",
            profileImg : "/ppCity.jpg",
        },
        information : {
            nickName : "The Citizens",
            founded : "16 April 1894",
            ground : "City of Manchester Stadium",
            capacity : 	53400,
            owner : "City Football Group",
            chairman : "Khaldoon Al Mubarak",
            headCoach : "Pep Guardiola",
            league : "Premier League",
            website : "http://www.mancity.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('011'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/barca.jpeg",
                content : "Ini Content"
            },            {
                post_id : parseInt('014'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/barca2.jpeg",
                content : "Ini Content"
            }
        ],
        following: false
    },
    {
        id : parseInt('008'),
        name : "Real Madrid",
        nickName : "Real Madrid",
        logo : "/madrid.png",
        profile : {
            motto : "‘Superbia in proelia’",
            headerImg : "/hCity.jpg",
            profileImg : "/ppCity.jpg",
        },
        information : {
            nickName : "The Citizens",
            founded : "16 April 1894",
            ground : "City of Manchester Stadium",
            capacity : 	53400,
            owner : "City Football Group",
            chairman : "Khaldoon Al Mubarak",
            headCoach : "Pep Guardiola",
            league : "Premier League",
            website : "http://www.mancity.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('013'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real.jpg",
                content : "Ini Content"
            },            {
                post_id : parseInt('012'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real2.jpg",
                content : "Ini Content"
            }
        ],
        following: false
    },
    {
        id : parseInt('009'),
        name : "Florian Neuhaus",
        nickName : "Neuhaus",
        logo : "/neuhaus.jpg",
        profile : {
            motto : "‘Superbia in proelia’",
            headerImg : "/hCity.jpg",
            profileImg : "/ppCity.jpg",
        },
        information : {
            nickName : "The Citizens",
            founded : "16 April 1894",
            ground : "City of Manchester Stadium",
            capacity : 	53400,
            owner : "City Football Group",
            chairman : "Khaldoon Al Mubarak",
            headCoach : "Pep Guardiola",
            league : "Premier League",
            website : "http://www.mancity.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('013'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real.jpg",
                content : "Ini Content"
            },            {
                post_id : parseInt('012'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real2.jpg",
                content : "Ini Content"
            }
        ],
        following: true
    },
    {
        id : parseInt('010'),
        name : "Gian Piero Gasperini",
        nickName : "Gasperini",
        logo : "/gasperini.png",
        profile : {
            motto : "‘Superbia in proelia’",
            headerImg : "/hCity.jpg",
            profileImg : "/ppCity.jpg",
        },
        information : {
            nickName : "The Citizens",
            founded : "16 April 1894",
            ground : "City of Manchester Stadium",
            capacity : 	53400,
            owner : "City Football Group",
            chairman : "Khaldoon Al Mubarak",
            headCoach : "Pep Guardiola",
            league : "Premier League",
            website : "http://www.mancity.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('013'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real.jpg",
                content : "Ini Content"
            },            {
                post_id : parseInt('012'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real2.jpg",
                content : "Ini Content"
            }
        ],
        following: true
    },
    {
        id : parseInt('011'),
        name : "Erik ten Hag",
        nickName : "Ten Hag",
        logo : "/tenhag.jpg",
        profile : {
            motto : "‘Superbia in proelia’",
            headerImg : "/hCity.jpg",
            profileImg : "/ppCity.jpg",
        },
        information : {
            nickName : "The Citizens",
            founded : "16 April 1894",
            ground : "City of Manchester Stadium",
            capacity : 	53400,
            owner : "City Football Group",
            chairman : "Khaldoon Al Mubarak",
            headCoach : "Pep Guardiola",
            league : "Premier League",
            website : "http://www.mancity.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('013'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real.jpg",
                content : "Ini Content"
            },            {
                post_id : parseInt('012'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real2.jpg",
                content : "Ini Content"
            }
        ],
        following: true
    },
    {
        id : parseInt('012'),
        name : "César Azpilicueta Tanco",
        nickName : "Azpilicueta",
        logo : "/azpi.webp",
        profile : {
            motto : "‘Superbia in proelia’",
            headerImg : "/hCity.jpg",
            profileImg : "/ppCity.jpg",
        },
        information : {
            nickName : "The Citizens",
            founded : "16 April 1894",
            ground : "City of Manchester Stadium",
            capacity : 	53400,
            owner : "City Football Group",
            chairman : "Khaldoon Al Mubarak",
            headCoach : "Pep Guardiola",
            league : "Premier League",
            website : "http://www.mancity.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('013'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real.jpg",
                content : "Ini Content"
            },            {
                post_id : parseInt('012'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real2.jpg",
                content : "Ini Content"
            }
        ],
        following: true
    },
    {
        id : parseInt('013'),
        name : "Kai Havertz",
        nickName : "Havertz",
        logo : "/kai.jpg",
        profile : {
            motto : "‘Superbia in proelia’",
            headerImg : "/hCity.jpg",
            profileImg : "/ppCity.jpg",
        },
        information : {
            nickName : "The Citizens",
            founded : "16 April 1894",
            ground : "City of Manchester Stadium",
            capacity : 	53400,
            owner : "City Football Group",
            chairman : "Khaldoon Al Mubarak",
            headCoach : "Pep Guardiola",
            league : "Premier League",
            website : "http://www.mancity.com/"
        },
        achievements: [
            {
                id: "",
                image: "",
                name: "",
                times: ""
            }
        ],
        posts: [
            {
                post_id : parseInt('013'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real.jpg",
                content : "Ini Content"
            },            {
                post_id : parseInt('012'),
                category: "season-review",
                title : "Season Review 2020/2021",
                desc : `“Chelsea harus berjuang mengalahkan Brentford dalam duel lanjutan Premier League akhir pekan lalu (16/10/2021)  tetaplah kemenangan. ”`,
                author : "Anto Kewer",
                thumbnail : "/real2.jpg",
                content : "Ini Content"
            }
        ],
        following: true
    }
]

export default accounts;