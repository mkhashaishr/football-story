import React from "react";
import { Navbar, Container, Nav, Button, InputGroup, FormControl, Dropdown, Stack } from "react-bootstrap";
import { NavLink, useHistory } from "react-router-dom";
import logo from '../Assets/Img/Bola.svg';
import Cookies from "js-cookie";

const Header = () => {

    let history = useHistory()
    const handleLogout = () => {
      Cookies.remove('token')
      Cookies.remove('name')
      Cookies.remove('email')  
      history.push('/login')
    }

    return (
        <>
            <Navbar bg="white" collapseOnSelect expand="lg" variant="light" className="fixed-top navigation-bar" style={{borderBottom : "1px solid #E2E8F0"}}>
                <Container className="g-lg-0">
                    <Navbar.Brand className="nav-title">
                        {
                            Cookies.get('token') !== undefined && 
                            <NavLink to="/home" className="text-decoration-none">
                                <img src={logo} alt="logo" width="20px" style={{marginTop: "-0.3rem"}}/>
                                <span className="mx-2 text-primary">FStory</span>
                            </NavLink>
                        }
                        {
                            Cookies.get('token') === undefined && 
                            <NavLink to="/" className="text-decoration-none">
                                <img src={logo} alt="logo" width="20px" style={{marginTop: "-0.3rem"}}/>
                                <span className="mx-2 text-primary">FStory</span>
                            </NavLink>
                        }
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">

                        {
                            Cookies.get('token') !== undefined &&
                            <>
                                <Nav className="ms-auto">
                                    <InputGroup className="search-bar my-2 my-lg-0">
                                        <InputGroup.Text id="basic-addon1" className="px-2" style={{borderRight: 'none', borderTopLeftRadius: "7px",borderBottomLeftRadius: "7px", borderColor: "#CBD5E0", backgroundColor: 'white', color: "#A0AEC0"}}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                            </svg>
                                        </InputGroup.Text>
                                        <FormControl
                                            className="search-bar--text"
                                            style={{color: "#2D3748",borderLeft: 'none', borderTopRightRadius: "7px",borderBottomRightRadius: "7px",borderColor: "#CBD5E0", fontFamily: "'Roboto', sans-serif,", fontSize: "0.875rem"}}
                                            placeholder="Search Clubs and Players... "
                                            aria-label="Search"
                                            aria-describedby="basic-addon1"
                                            className="px-1"
                                        />
                                    </InputGroup>
                                </Nav>
                            </>

                        }

                        <Nav className="ms-auto my-2 my-lg-0">

                            {
                                Cookies.get('token') === undefined &&                            
                                <>
                                    <NavLink activeClassName="active" to="/register">
                                        <Button variant="outline-secondary" className="btnb scnd px-3">Register</Button>
                                    </NavLink>
                                    <NavLink to="/login">
                                        <Button variant="primary" className="btnb px-4 text-white">Login</Button>
                                    </NavLink>
                                </>
                            }

                            {
                                Cookies.get('token') !== undefined &&  
                                <>
                                    <Stack spacing={5} className="d-lg-none menu-mobile">
                                        <NavLink activeClassName="active" style={{marginBottom: "0.5rem"}} to="/home">
                                            <div className="d-inline-flex gap-2 align-items-center side-menu--wrapper">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-house-fill" viewBox="0 0 16 16">
                                                    <path fill-rule="evenodd" d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                                                    <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
                                                </svg>
                                                <span className="side-menu--wrapper--text">For You</span>
                                            </div>
                                        </NavLink>
                                        <NavLink activeClassName="active" style={{marginBottom: "0.5rem"}} to="/following">
                                            <div className="d-inline-flex gap-2 align-items-center side-menu--wrapper">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                                                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                                </svg>
                                                <span className="side-menu--wrapper--text">Following</span>
                                            </div>
                                        </NavLink>
                                        <NavLink activeClassName="active" style={{marginBottom: "0.5rem"}} to="/saved">
                                            <div className="d-inline-flex gap-2 align-items-center side-menu--wrapper">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-bookmark-fill" viewBox="0 0 16 16">
                                                    <path d="M2 2v13.5a.5.5 0 0 0 .74.439L8 13.069l5.26 2.87A.5.5 0 0 0 14 15.5V2a2 2 0 0 0-2-2H4a2 2 0 0 0-2 2z"/>
                                                </svg>
                                                <span className="side-menu--wrapper--text">Saved</span>
                                            </div>
                                        </NavLink>
                                        <NavLink activeClassName="active" style={{marginBottom: "0.5rem"}} to="/settings">
                                            <div className="d-inline-flex gap-2 align-items-center side-menu--wrapper">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-gear-fill" viewBox="0 0 16 16">
                                                    <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
                                                </svg>
                                                <span className="side-menu--wrapper--text">Settings</span>
                                            </div>
                                        </NavLink>
                                        <NavLink activeClassName="active" style={{marginBottom: "0.5rem"}} onClick={handleLogout} to>
                                            <div className="d-inline-flex gap-2 align-items-center side-menu--wrapper">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-power" viewBox="0 0 16 16">
                                                    <path d="M7.5 1v7h1V1h-1z"/>
                                                    <path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812z"/>
                                                </svg>
                                                <span className="side-menu--wrapper--text">Logout</span>
                                            </div>
                                        </NavLink>
                                    </Stack>
                                    <Dropdown align="end" className="d-none d-lg-flex">
                                        <Dropdown.Toggle variant="white" className="border-1 border-gray-300 py-1 px-2" id="dropdown-menu-align-end">
                                            <img src="https://deadmau5.com/wp/wp-content/themes/deadmau5/images/OG_image.jpg" alt="Profile" style={{marginRight: "0.25rem", borderRadius: "50%", objectFit: "cover",width: "25px",height: "25px", display: "inline-block"}}/>
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu>
                                            <Dropdown.Item href="#/action-1">Settings</Dropdown.Item>
                                            <Dropdown.Item onClick={handleLogout}>Logout</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </>
                            }

                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    )
}

export default Header;