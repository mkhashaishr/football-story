import React from "react";
import { Col, Container, Row, Button, Card} from "react-bootstrap";
import accounts from '../../Assets/data/data';

const Explore = () => {

    return (
        <>
            <Container className="explore-section">
                <Row className="text-center mb-5">
                    <Col sm="12">
                        <Row className="justify-content-center text-center">                    
                            <h2 className="about-section--title text-gray-700">Explore Clubs & Players</h2>
                        </Row>
                        <Row className="justify-content-center">
                            <p className="about-section--desc text-gray-500 text-center" style={{marginBottom: "0rem"}}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Curabitur fringilla quis velit at lacinia.
                            </p>         
                        </Row>
                    </Col>
                </Row>
                <Row className="g-0 justify-content-center align-items-center">
                    <Col sm="1" className="d-none d-md-flex justify-content-center">
                        <button className="btn-scroll p-0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="#319795" className="bi bi-arrow-left-circle-fill" viewBox="0 0 16 16">
                                <path d="M8 0a8 8 0 1 0 0 16A8 8 0 0 0 8 0zm3.5 7.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"/>
                            </svg>
                        </button>    
                    </Col>
                    <Col sm="10">
                        <Card className="card-explore">
                            <Card.Body className="py-0">
                                <div className="row flex-row flex-nowrap" style={{overflowX: "auto"}}>
                                    {accounts !== null && (
                                        <>
                                            {accounts.map((e) => (
                                                <Card className="text-center border-0" style={{width : "9rem"}} key={e.id}>
                                                    <Card.Body className="g-0">
                                                        <img alt="Club Logo" src={require('../../Assets/Img/club-logo' + e.logo).default}  className="img-fluid img-logo" />
                                                        <p className="nickname">{e.nickName}</p>
                                                        <Button variant="outline-primary" className="btnb explore px-4">Follow</Button>
                                                    </Card.Body>
                                                </Card>
                                            ))}
                                        </>
                                    )}                                    
                                </div>
                            </Card.Body>
                        </Card>   
                    </Col>
                    <Col sm="1" className="d-none d-md-flex justify-content-center">
                        <button className="btn-scroll p-0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="#319795" className="bi bi-arrow-right-circle-fill" viewBox="0 0 16 16">
                                <path d="M8 0a8 8 0 1 1 0 16A8 8 0 0 1 8 0zM4.5 7.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"/>
                            </svg>
                        </button>      
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default Explore;