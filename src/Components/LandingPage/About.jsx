import React from "react";
import { Container, Row, Col, Stack, Image } from "react-bootstrap";
import img1 from '../../Assets/Img/1.png';
import img2 from '../../Assets/Img/2.png';
import img3 from '../../Assets/Img/3.png';

const About = () => {
    return (
        <div className="about-section">
            <Container className="g-lg-0">
                <Stack>
                    <Row>
                        <Col sm="12">
                            <Row className="justify-content-center text-center">                    
                                <h2 className="about-section--title text-gray-700">One App for All</h2>
                            </Row>
                            <Row className="justify-content-center">
                                <p className="about-section--desc text-gray-500 text-center">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Curabitur fringilla quis velit at lacinia.
                                </p>         
                            </Row>
                        </Col>
                    </Row>
                    <Row className="align-items-center about-section--wrapper justify-content-start d-flex">
                        <Col lg="5" className="order-2 order-lg-1">
                            <h2 className="about-section--title-2 text-gray-700">Read Story About Clubs</h2>
                            <p className="about-section--content ac1 text-gray-500 text-start">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla quis velit at lacinia.
                                Suspendisse potenti. In et est elit. Aliquam mauris elit, rutrum at dignissim ac, gravida sed libero. 
                                Sed dictum diam aliquam est varius tristique. 
                            </p>
                        </Col>
                        <Col lg="5" className="order-1 order-lg-2 d-flex justify-content-center justify-content-lg-end p-0">
                            <Image src={img1} className="img-fluid float-md-end" />
                        </Col>
                    </Row>
                    <Row className="align-items-center about-section--wrapper justify-content-end">
                        <Col lg="5" className="d-flex justify-content-center justify-content-xl-start p-0">
                            <Image src={img2} className="img-fluid " />
                        </Col>
                        <Col lg="5">
                            <div className="float-start about-section--padding">
                                <h2 className="about-section--title-2 text-gray-700 ">Read Story About Players</h2>
                                <p className="about-section--content ac2 text-gray-500 text-start">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla quis velit at lacinia.
                                    Suspendisse potenti. In et est elit. Aliquam mauris elit, rutrum at dignissim ac, gravida sed libero. 
                                    Sed dictum diam aliquam est varius tristique. 
                                </p>
                            </div>
                        </Col>
                    </Row>
                    <Row className="align-items-center about-section--wrapper justify-content-center d-flex">
                        <Col lg="5" className="d-flex justify-content-center justify-content-xl-end p-0 order-1 order-lg-2">
                            <Image src={img3} className="img-fluid" />
                        </Col>
                        <Col lg="5" className="order-2 order-lg-1">
                            <h2 className="about-section--title-2 text-gray-700">Read Story About Competition</h2>
                            <p className="about-section--content ac3 text-gray-500 text-start">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla quis velit at lacinia.
                                Suspendisse potenti. In et est elit. Aliquam mauris elit, rutrum at dignissim ac, gravida sed libero. 
                                Sed dictum diam aliquam est varius tristique. 
                            </p>
                        </Col>
                    </Row>
                </Stack>
            </Container>
        </div>
    )
}

export default About;