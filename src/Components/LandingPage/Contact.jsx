import React from "react";
import { Container, Row, Col, Card, Form, Button } from "react-bootstrap";

const Contact = () => {
    return (
        <div className="contact-section">
            <Container className="g-lg-0">
                <Row className="align-items-center justify-content-center">
                    <Col lg="5">
                        <Row>
                            <Col sm="12">
                                <Row className="justify-content-start text-start px-2 px-md-0">                    
                                    <h2 className="about-section--title text-gray-700 px-md-0">Contact</h2>
                                </Row>
                                <Row className="justify-content-start">
                                    <p className="contact-section--desc text-gray-500 text-start px-md-0">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Curabitur fringilla quis velit at lacinia.
                                    </p>         
                                </Row>
                            </Col>
                        </Row>                      
                    </Col>
                    <Col lg="5">
                        <Card className="card-contact" >
                            <Card.Body className="py-5 px-4">
                                <Form className="contact-form">
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control type="text" placeholder="Enter your name" className="placeholder-border" />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control type="email" placeholder="name@example.com" className="placeholder-border" />
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Message</Form.Label>
                                        <Form.Control as="textarea" placeholder="Message" rows={5} className="placeholder-border" />
                                    </Form.Group>
                                    <Button className="btnb px-4 py-2 text-white mt-2">Submit</Button>
                                </Form>
                            </Card.Body>
                        </Card>                     
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Contact;
