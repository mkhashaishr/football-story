import React from "react";
import { Container, Row, Col, Stack } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import imgFooter from '../../Assets/Img/BolaF.svg'
import socmed1 from '../../Assets/Img/footer/1.svg'
import socmed2 from '../../Assets/Img/footer/2.svg'
import socmed3 from '../../Assets/Img/footer/3.svg'
import socmed4 from '../../Assets/Img/footer/4.svg'


const Footer = () => {
    return (
        <>
            <Container className="footer-section g-lg-0">
                <Row>
                    <Col lg="6" className="mb-4 md-lg-0">
                        <div className="footer-section--header--1 align-items-center p-0 gx-0 d-flex">                            
                            <img alt="Footer" src={imgFooter} />
                            <h3 className="footer-section--header fh p-0 g-0 ms-1">FStory</h3>                           
                        </div>
                        <p className="footer-section--text p-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ut aliquet velit, in consectetur lorem.</p>
                    </Col>
                    <Col lg="2" className="mb-4 md-lg-0">
                        <h3 className="footer-section--header">Subscribe</h3>
                        <ul className="no-bullets">
                            <li>
                                <NavLink className="link-footer" to="/">
                                    Start Free Trial
                                </NavLink>
                            </li>
                            <li>
                                <NavLink className="link-footer" to="/">
                                    Student Discount
                                </NavLink>
                            </li>
                            <li>
                                <NavLink className="link-footer" to="/">
                                    Invite Friends
                                </NavLink>
                            </li>
                        </ul>
                    </Col>
                    <Col lg="2" className="mb-4 md-lg-0">
                        <h3 className="footer-section--header">Support</h3>
                        <ul className="no-bullets">
                            <li>
                                <NavLink className="link-footer" to="/">
                                    FAQ
                                </NavLink>
                            </li>
                            <li>
                                <NavLink className="link-footer" to="/">
                                    Forgot Password ?
                                </NavLink>
                            </li>
                            <li>
                                <NavLink className="link-footer" to="/">
                                    Redeem Gift
                                </NavLink>
                            </li>
                            <li>
                                <NavLink className="link-footer" to="/">
                                    Email Us
                                </NavLink>
                            </li>
                        </ul>
                    </Col>
                    <Col lg="2" className="mb-4 md-lg-0">
                        <h3 className="footer-section--header">HQ</h3>
                        <ul className="no-bullets">
                            <li>
                                <NavLink className="link-footer" to="/">
                                    Careers
                                </NavLink>
                            </li>
                            <li>
                                <NavLink className="link-footer" to="/">
                                    Code of Conduct
                                </NavLink>
                            </li>
                            <li>
                                <NavLink className="link-footer" to="/">
                                    Business Inquiries
                                </NavLink>
                            </li>
                            <li>
                                <NavLink className="link-footer" to="/">
                                    Press Inquiries
                                </NavLink>
                            </li>
                        </ul>
                    </Col>
                </Row>
                <Row className="mt-2 mt-lg-5 g-0">
                    <Stack direction="horizontal">
                        <p className="copyright mt-3">2021 <b>FStory</b> inc. all rights reserved </p>
                        <Stack direction="horizontal" className="ms-auto" gap={4}>
                            <NavLink to="/">
                                <img src={socmed1} alt="Facebook" />
                            </NavLink>
                            <NavLink to="/">
                                <img src={socmed2} alt="Twitter" />    
                            </NavLink>
                            <NavLink to="/">
                                <img src={socmed3} alt="Instagram" />
                            </NavLink>
                            <NavLink to="/">
                                <img src={socmed4} alt="Whatsapp" />
                            </NavLink>
                        </Stack>
                    </Stack>
                </Row>
            </Container>
        </>
    )
}

export default Footer;