import React from "react";
import { Col, Container, Row, Stack, Button } from "react-bootstrap";
import imgHero from '../../Assets/Img/hero.png'

const Hero = () => {
    return (
        <div className="hero-section">
            <Container className="g-lg-0">
                <Row className="justify-content-center">
                    <Stack>
                        <h1 className="text-gray-700 text-center hero-section--header">Read the best story from </h1>
                        <h1 className="text-primary text-center hero-section--header">Football World</h1>
                    </Stack>
                </Row>
                <Row className="justify-content-center">
                    <Col lg="7">
                        <h3 className="text-gray-500 text-center hero-section--desc">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla quis velit at lacinia. 
                            Suspendisse potenti. In et est elit. Aliquam mauris elit, rutrum at dignissim ac, gravida sed libero. 
                            Sed dictum diam aliquam est varius tristique. Nunc commodo ultrices velit id hendrerit. 
                        </h3>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Stack gap={2} className="col-md-2 mx-auto">
                            <Button variant="primary" className="btnb py-3  text-white">Get Started</Button>
                            <Button variant="outline-secondary" className="btnb scnd py-3">Learn More</Button>
                        </Stack>
                    </Col>
                </Row>
                <Row className="justify-content-md-center">
                    <Col md="auto">
                        <img src={imgHero} alt="Hero" className="img-fluid hero-section--img"/>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Hero;