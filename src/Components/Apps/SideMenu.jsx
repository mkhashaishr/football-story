import React from "react";
import { Stack } from "react-bootstrap";
import imgNeuhaus from "../../Assets/Img/club-logo/neuhaus.jpg";
import { NavLink } from "react-router-dom";
import accounts from "../../Assets/data/data";

const SideMenu = () => {

    let following = accounts.filter(p => p.following === (true));

    return (
        <>
            <Stack className="p-0 gap-2 side-menu d-none d-lg-flex">
                <NavLink activeClassName="active" to="/home">
                    <div className="d-inline-flex gap-2 align-items-center side-menu--wrapper">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-house-fill" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                            <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
                        </svg>
                        <span className="side-menu--wrapper--text">For You</span>
                    </div>
                </NavLink>
                <NavLink activeClassName="active" to="/following">
                    <div className="d-inline-flex gap-2 align-items-center side-menu--wrapper">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                            <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                        </svg>
                        <span className="side-menu--wrapper--text">Following</span>
                    </div>
                </NavLink>
                <NavLink activeClassName="active" to="/saved">
                    <div className="d-inline-flex gap-2 align-items-center side-menu--wrapper">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-bookmark-fill" viewBox="0 0 16 16">
                            <path d="M2 2v13.5a.5.5 0 0 0 .74.439L8 13.069l5.26 2.87A.5.5 0 0 0 14 15.5V2a2 2 0 0 0-2-2H4a2 2 0 0 0-2 2z"/>
                        </svg>
                        <span className="side-menu--wrapper--text">Saved</span>
                    </div>
                </NavLink>
                <hr style={{width: '80%'}} />
                <span>Following</span>
                {following.map((e) => (
                    <NavLink activeClassName="following" to="#">
                        <div className="d-inline-flex gap-2 align-items-center side-menu--wrapper" key={e.id}>
                            <div className="side-menu--wrapper--img">
                                <img src={require('../../Assets/Img/club-logo' + e.logo).default}alt="Profile Logo" />
                            </div>
                            <span className="side-menu--wrapper--text">{e.name}</span>
                        </div>
                    </NavLink>
                ))}
                <hr style={{width: '80%'}} />
                <div className="d-flex flex-wrap gap-2 side-menu--footer" style={{width: '80%'}}>
                    <NavLink to="#">About</NavLink>
                    <NavLink to="#">Newsroom</NavLink>
                    <NavLink to="#">Contact</NavLink>
                    <NavLink to="#">Privacy Policy</NavLink>
                    <NavLink to="#">© <b>FStory</b> 2021</NavLink>
                </div>
            </Stack>
        </>
    )
}

export default SideMenu;