import React from "react";
import { Container, Row, Col, Stack, Card, Button, Nav } from "react-bootstrap";
import arrowLeft from '../../Assets/Img/icons/arrow-left.png';
import saved from '../../Assets/Img/icons/bookmark.png';
import accounts from '../../Assets/data/data';
import { NavLink } from "react-router-dom";

const CpContent = () => {

    let obj = accounts.find(e => e.id === parseInt('001'));

    return (
        <>
            <Container className="g-0 p-0 cp-content">
                <Row className="g-0">
                    <Col sm="12">
                        <Stack className="text-center">
                            <Card className="card-content">
                                <Card.Body>
                                    <Row className="px-lg-3">
                                        <Col>
                                            <div className="d-flex justify-content-between align-items-center"> 
                                                <div className="btn-top">
                                                    <NavLink to="/home">
                                                        <img src={arrowLeft} alt="Arrow Left Icon" />
                                                    </NavLink>
                                                </div>
                                                <div className="btn-top">
                                                    <img src={saved}  alt="Save Icon" />
                                                </div>                                        
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row className="justify-content-center card-content--header">
                                        <Col lg="7">
                                            <Stack>
                                            <h1 className="card-content--header--title">{obj.posts[1].title}</h1>
                                            <p className="card-content--header--desc">{obj.posts[1].desc}</p>
                                            <p className="card-content--header--author">by <b>{obj.posts[1].author} </b></p>
                                            </Stack>
                                        </Col>
                                    </Row>
                                    <Row className="px-lg-3 justify-content-center">
                                        <Col xl="10">
                                            <img src={require('../../Assets/Img/club-post' + obj.posts[1].thumbnail).default} alt="Article" className="img-fluid header-img" />
                                        </Col>
                                    </Row>
                                    <Row className="px-lg-3 text-start">
                                        <Col>
                                            <p className="card-content--text">
                                                {obj.posts[1].content}
                                            </p>
                                            <p className="card-content--text">
                                                {obj.posts[1].content}
                                            </p>
                                            <p className="card-content--text">
                                                {obj.posts[1].content}
                                            </p>
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Stack>
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default CpContent;