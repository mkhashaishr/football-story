import React from "react";
import { Row, Col, Stack, Button, Image } from "react-bootstrap";
import accounts from "../../Assets/data/data";
import twitter from "../../Assets/Img/socmed/twitter.svg"
import ig from "../../Assets/Img/socmed/ig.svg"
import fb from "../../Assets/Img/socmed/fb.svg"
import wa from "../../Assets/Img/socmed/wa.svg"
import { NavLink } from "react-router-dom";

const CpSidebarL = () => {

    let obj = accounts.find(e => e.id === parseInt('001'));

    return (
        <div className="cp-sidebar-l">
            <Stack gap={5}>
                <div className="cp-sidebar-l--follow">
                    <Row>
                        <Col className="d-flex justify-content-center">
                            <img src={require('../../Assets/Img/club-logo' + obj.logo).default} alt="Chelsea" className="img-fluid cp-sidebar-l--follow-img" />
                        </Col>
                    </Row>
                    <Row >
                        <Col className="d-flex justify-content-center h-auto">
                            <p className="cp-sidebar-l--follow--clubname text-center">{obj.name}</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="d-flex justify-content-center">
                            <Button className="btnb text-white cp-sidebar-l--follow--btn">Following</Button>
                        </Col>
                    </Row>
                </div>
                <div className="cp-sidebar-l--share">
                    <Row>
                        <Col className="d-flex justify-content-center">
                            <p className="cp-sidebar-l--share--title">Share</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="d-flex justify-content-center">
                            <ul className="cp-sidebar-l--share--list">
                                <li>
                                    <NavLink to={{pathname:"https://twitter.com"}} target="_blank">
                                        <Image src={twitter} alt="Twitter" />
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink to={{pathname:"https://instagram.com"}} target="_blank">
                                        <Image src={ig} alt="Instagram" />
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink to={{pathname:"https://facebook.com"}} target="_blank">
                                        <Image src={fb} alt="Facebook" />
                                    </NavLink>
                                </li>
                                <li>
                                    <NavLink to={{pathname:"https://web.whatsapp.com/"}} target="_blank">
                                        <Image src={wa} alt="Whatsapp" />
                                    </NavLink>
                                </li>
                            </ul>
                        </Col>
                    </Row>
                </div>
            </Stack>
        </div>
    )
}

export default CpSidebarL;