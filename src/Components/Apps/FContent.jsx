import React from "react";
import { Stack, Card, Row, Col, Button } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import accounts from "../../Assets/data/data";

const FContent = () => {

    let following = accounts.filter(p => p.following === (true));

    return (
        <>                                                              
            <Card className="card-explore px-4">
                <Card.Body>
                    {following.map((e) => (     
                        <>
                            {e.posts.map((p)=> (
                                <>
                                    <div className="d-block d-lg-flex" style={{marginTop: "2rem"}}>
                                        <div className="d-flex justify-content-center g-0 p-0">
                                            <img className="card-explore--thumbnail" alt="Home" src={require('../../Assets/Img/club-post' + p.thumbnail).default} />
                                        </div>
                                        <div className="w-100 d-block card-explore--text">
                                            <div className="d-flex align-items-center">                                               
                                                <div className="d-inline-flex align-items-center gap-2">
                                                    <img className="card-explore--logo" style={{height: "35px", width: "35px", objectFit: "cover", borderRadius: "50%"}} src={require('../../Assets/Img/club-logo' + e.logo).default} alt="Club Logo" />
                                                    <p className="card-explore--name">{e.nickName}</p>
                                                </div>
                                                <div className="ms-auto">
                                                    <Button variant="primary" className="btnb explore px-3 text-white">Following</Button>
                                                </div>
                                            </div>
                                            <hr style={{marginTop: "0.5rem"}} />
                                            <Row className="p-0 g-0">
                                                <Col>
                                                    <Stack>
                                                        <h1 className="card-explore--title">{p.title}</h1>
                                                        <p className="card-explore--content text-start">{p.desc.substring(0, 150)}</p>
                                                        <NavLink className="card-explore--link" to="/">
                                                            Read More
                                                        </NavLink>
                                                    </Stack>
                                                </Col>
                                            </Row>
                                        </div>
                                    </div> 
                                    <hr style={{marginTop: "2rem"}} />   
                                </>
                            ))}
                        </>
                    ))}  
                </Card.Body>
            </Card>                                                      
        </>
    )
}

export default FContent;