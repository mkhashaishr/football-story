import React from "react";
import { Card, Button } from "react-bootstrap";
import accounts from "../../Assets/data/data";

const HExplore = () => {

    let fyp = accounts.filter(p => p.following !== (true));

    return (
        <>
            <Card className="card-explore">
                <Card.Body className="py-0">
                    <div className="row flex-row flex-nowrap" style={{overflowX: "auto"}}>
                        {fyp !== null && (
                            <>
                                {fyp.map((e) => (
                                    <Card className="text-center border-0" style={{width : "9rem"}} key={e.id}>
                                        <Card.Body className="g-0">
                                            <img src={require('../../Assets/Img/club-logo' + e.logo).default} style={{borderRadius: "50%", objectFit: "cover"}} className="img-fluid img-logo" />
                                            <p className="nickname">{e.nickName}</p>
                                            <Button variant="outline-primary" className="btnb explore px-4">Follow</Button>
                                        </Card.Body>
                                    </Card>
                                ))}
                            </>
                        )}                                    
                    </div>
                </Card.Body>
            </Card> 
        </>
    )
}

export default HExplore;