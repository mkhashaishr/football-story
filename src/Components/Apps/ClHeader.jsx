import React from "react";
import {Row, Col, Card, Stack, Button} from "react-bootstrap";
import imgHeader from "../../Assets/Img/club-header/hChelsea.jpg";
import imgPl from "../../Assets/Img/club-competition/pl.png";
import imgFa from "../../Assets/Img/club-competition/fa.png";
import imgEfl from "../../Assets/Img/club-competition/efl.png";
import imgCs from "../../Assets/Img/club-competition/cs.png";
import imgUcl from "../../Assets/Img/club-competition/ucl.png";
import imgUel from "../../Assets/Img/club-competition/uel.png";
import imgSc from "../../Assets/Img/club-competition/sc.png";
import imgCwc from "../../Assets/Img/club-competition/cwc.png"
import accounts from "../../Assets/data/data";



const ClHeader = () => {

    let obj = accounts.find(e => e.id === parseInt('001'));

    return (
        <div className="cl-header">
            <Card style={{borderRadius: "10px"}}>
                <Card.Body className="g-0 p-0">
                    <Stack>
                        <div className="cl-header--wrapper position-relative">
                            <div className="cl-header--wrapper--img">
                                <img src={imgHeader} alt="Club Header" className="img-fluid" />
                            </div>
                            <div className="cl-header--wrapper--top">
                                <div className="cl-header--wrapper--top--cp ">
                                    <span>Club Popularity : <b>#3</b></span>
                                </div>
                                <div className="cl-header--wrapper--top--follow">
                                    <Button variant="primary" className="btnb explore px-3 text-white">Following</Button>
                                </div>
                            </div>
                            <Stack className="position-absolute top-50 start-50 translate-middle w-50 cl-header--wrapper--center text-center">
                                <div>
                                    <img src="https://thumbs.dreamstime.com/b/chelsea-club-logo-blue-197167454.jpg" alt="hehe" style={{width: "130px", height: "130px", borderRadius: "50%",border: "1px solid #E2E8F0",objectFit: "cover"}} />
                                </div>
                                <span className="club-name">Chelsea Football Club</span>
                                <span className="club-moto">‘Nisi Dominus Frustra’</span>
                            </Stack>
                        </div>
                        <Stack direction="horizontal" className="m-0 px-3 px-xl-1 m-lg-4 cl-header--compe d-inline-flex align-items-center flex-nowrap overflow-auto">
                            {obj.achievements.map((p)=> (
                                <>
                                    <div className="cl-header--compe--item" key={p.id}>
                                        <Stack className="text-center">
                                            <div className="cl-header--compe--item--img">
                                                <img src={require('../../Assets/Img/club-competition' + p.image).default} alt="hehe" style={{width: "auto", height: "60px",objectFit: "cover"}} />
                                            </div>
                                            <span className="cl-header--compe--item--cname">{p.name}</span>
                                            <span className="cl-header--compe--item--ctimes">{p.times}</span>
                                        </Stack>
                                    </div>
                                </>
                            ))}
                        </Stack>
                    </Stack>
                </Card.Body>
            </Card>
        </div>
    )
}

export default ClHeader;