import React from "react";
import { Card, Nav, Col, Row, Stack } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import thumbnail from '../../Assets/Img/club-post/ajax.jpeg';
import accounts from "../../Assets/data/data";

const CiContent = () => {

    let obj = accounts.find(e => e.id === parseInt('001'));
    let sr = obj.posts.filter(p => p.category === ('season-review'));
    let intro = obj.posts.filter(p => p.category === ('intro'));
    let news = obj.posts.filter(p => p.category === ('news'));

    return (
        <div className="cl-content">
            <Card className="cl-content--card">
                <Card.Body>
                    <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist" style={{color: "tomato"}}>
                        <li className="nav-item" role="presentation">
                            <button className="nav-link px-4 cl-content--card--pill active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">All</button>
                        </li>
                        <li className="nav-item" role="presentation">
                            <button className="nav-link px-4 cl-content--card--pill" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Introduction</button>
                        </li>
                        <li className="nav-item" role="presentation">
                            <button className="nav-link px-4 cl-content--card--pill" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Season Story</button>
                        </li>
                        <li className="nav-item" role="presentation">
                            <button className="nav-link px-4 cl-content--card--pill" id="pills-news-tab" data-bs-toggle="pill" data-bs-target="#pills-news" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">News</button>
                        </li>
                    </ul>
                    <div className="tab-content" id="pills-tabContent">
                        <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div  className="d-inline-flex gap-2 flex-wrap w-100 g-0 cl-content--card--cwrapper">
                                {obj.posts.map((e) => (                                    
                                    <div key={e.post_id} className="cl-content--card--cwrapper--card">
                                        <Stack>                                   
                                            <img src={require('../../Assets/Img/club-post' + e.thumbnail).default} className="cl-content--card--cwrapper--card--img" alt="Title" />
                                            <NavLink to="/content">
                                                <span className="cl-content--card--cwrapper--card--header">{e.title}</span>
                                            </NavLink>
                                            <span className="cl-content--card--cwrapper--card--text">{e.desc.substring(0,75) + '...'} </span>
                                        </Stack>
                                    </div>                                    
                                ))}
                            </div>
                        </div>
                        <div className="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div className="d-inline-flex gap-2 flex-wrap w-100 g-0 cl-content--card--cwrapper">
                                {intro.map((e) => (                                    
                                    <div key={e.post_id} className="cl-content--card--cwrapper--card">
                                        <Stack>                                   
                                            <img src={require('../../Assets/Img/club-post' + e.thumbnail).default} className="cl-content--card--cwrapper--card--img" alt="Title" />
                                            <NavLink to={`/content/${e.post_id}`}>
                                                <span className="cl-content--card--cwrapper--card--header">{e.title}</span>
                                            </NavLink>
                                            <span className="cl-content--card--cwrapper--card--text">{e.desc.substring(0,75) + '...'} </span>
                                        </Stack>
                                    </div>                                    
                                ))}
                            </div>
                        </div>
                        <div className="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div className="d-inline-flex gap-2 flex-wrap w-100 g-0 cl-content--card--cwrapper">
                                {sr.map((e) => (                                    
                                    <div key={e.post_id} className="cl-content--card--cwrapper--card">
                                        <Stack>                                   
                                            <img src={require('../../Assets/Img/club-post' + e.thumbnail).default} className="cl-content--card--cwrapper--card--img" alt="Title" />
                                            <NavLink to="/content">
                                                <span className="cl-content--card--cwrapper--card--header">{e.title}</span>
                                            </NavLink>
                                            <span className="cl-content--card--cwrapper--card--text">{e.desc.substring(0,75) + '...'} </span>
                                        </Stack>
                                    </div>                                    
                                ))}
                            </div>         
                        </div>
                        <div className="tab-pane fade" id="pills-news" role="tabpanel" aria-labelledby="pills-news-tab">
                            <div className="d-inline-flex gap-2 flex-wrap w-100 g-0 cl-content--card--cwrapper">
                                {news.map((e) => (                                    
                                    <div key={e.post_id} className="cl-content--card--cwrapper--card">
                                        <Stack>                                   
                                            <img src={require('../../Assets/Img/club-post' + e.thumbnail).default} className="cl-content--card--cwrapper--card--img" alt="Title" />
                                            <NavLink to={`/content/${e.post_id}`}>
                                                <span className="cl-content--card--cwrapper--card--header">{e.title}</span>
                                            </NavLink>
                                            <span className="cl-content--card--cwrapper--card--text">{e.desc.substring(0,75) + '...'} </span>
                                        </Stack>
                                    </div>                                    
                                ))}
                            </div>
                        </div>
                    </div>
                </Card.Body>
            </Card>
        </div>
    )
}

export default CiContent;