import React from "react";
import { Stack, Row, Col, Card, Nav } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import accounts from "../../Assets/data/data";

const CpSidebarR = () => {

    let obj = accounts.find(e => e.id === parseInt('001'));

    return (
        <div className="cp-sidebar-r">
            <Stack>
                <div className="cp-sidebar-r--title">
                    <Row>
                        <Col>
                            <h3>You may also like </h3>
                        </Col>
                    </Row>
                </div>
                <div className="cp-sidebar-r--article">
                    <ul>
                        {obj.posts.slice(0, 5).map((e) => (
                            <li key={e.post_id}>
                                <Card className="bg-gray-50 border-0">
                                    <NavLink to="/">
                                        <Card.Img className="cp-sidebar-r--article--thumbnail" variant="top" src={require('../../Assets/Img/club-post' + e.thumbnail).default} />
                                    </NavLink>
                                    <Card.Body className="p-0">
                                        <NavLink to="/" className="cp-sidebar-r--article--link">
                                            <Card.Title className="cp-sidebar-r--article--title">{e.title}</Card.Title>
                                        </NavLink>
                                        <Card.Text className="cp-sidebar-r--article--desc">
                                            {e.desc.substring(0,80) + '...'}
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </li>
                        ))}
                    </ul>
                </div>
            </Stack>
        </div>
    )
}

export default CpSidebarR;