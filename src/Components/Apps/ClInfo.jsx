import React from "react";
import { Card, Stack } from "react-bootstrap";
import accounts from "../../Assets/data/data";
import imgLogo from '../../Assets/Img/club-logo/chelsea.png';

const ClInfo = () => {

    let obj = accounts.find(e => e.id === parseInt('001'));

    return (
        <div className="cl-info d-none d-lg-block">
            <Card className="text-center" style={{borderRadius: "10px"}}>
                <Card.Body className="p-0 g-0">
                    <Stack>
                        <span className="my-2 cl-info--title">Club Information</span>
                        <div className="cl-info--img">
                            <img src={require('../../Assets/Img/club-logo' + obj.logo).default} alt={`${obj.name}Club Logo`} />
                        </div>
                        <ul>
                            <li>
                                <Stack>
                                    <span className="title">Full Name</span>
                                    <span className="info">{obj.name}</span>
                                </Stack>
                            </li>
                            <li>
                                <Stack>
                                    <span className="title">Nickname</span>
                                    <span className="info">{obj.nickName}</span>
                                </Stack>
                            </li>
                            <li>
                                <Stack>
                                    <span className="title">Ground</span>
                                    <span className="info">{obj.information.ground}</span>
                                </Stack>
                            </li>
                            <li>
                                <Stack>
                                    <span className="title">Capacity</span>
                                    <span className="info">{obj.information.capacity}</span>
                                </Stack>
                            </li>
                            <li>
                                <Stack>
                                    <span className="title">Owner</span>
                                    <span className="info">{obj.information.owner}</span>
                                </Stack>
                            </li>
                            <li>
                                <Stack>
                                    <span className="title">Chairman</span>
                                    <span className="info">{obj.information.chairman}</span>
                                </Stack>
                            </li>
                            <li>
                                <Stack>
                                    <span className="title">Head Coach</span>
                                    <span className="info">{obj.information.headCoach}</span>
                                </Stack>
                            </li>
                            <li>
                                <Stack>
                                    <span className="title">League</span>
                                    <span className="info">{obj.information.league}</span>
                                </Stack>
                            </li>
                            <li>
                                <Stack>
                                    <span className="title">Website</span>
                                    <span className="info">
                                        <a href={obj.information.website} target="_blank">Club Website</a>
                                    </span>
                                </Stack>
                            </li>
                        </ul>
                    </Stack>
                </Card.Body>
            </Card>
        </div>
    )
}

export default ClInfo;