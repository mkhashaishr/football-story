import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom"
import HomePage from "../Pages/HomePage";
import LandingPage from "../Pages/LandingPage";
import ClubPage from "../Pages/ClubPage";
import ContentPage from "../Pages/ContentPage";
import FollowingPage from "../Pages/FollowingPage";
import LoginPage from "../Pages/LoginPage";
import RegisterPage from "../Pages/Register";
import SavedPage from "../Pages/SavedPages";
import SettingsPage from "../Pages/Settings";
import Cookies from 'js-cookie';
import {Redirect} from 'react-router'

const Routes = () => {


    const LoginRoute = ({...props}) => {
        if(Cookies.get('token') !== undefined){  
          return <Redirect to="/" />
        } else {
          return <Route {...props} />
        }
      }
    
      const PrivateRoute = ({...props}) => {
        if(Cookies.get('token') === undefined){  
          return <Redirect to="/login" />
        } else {
          return <Route {...props} />
        }
      }

    return (
        <div>
            <Router>
                <Switch>
                    <Route path="/" exact>
                        <LandingPage />
                    </Route>

                    <PrivateRoute exact component={HomePage} path="/home" />
                    <PrivateRoute exact component={ClubPage} path="/club" />
                    <PrivateRoute exact component={ContentPage} path="/content" />
                    <PrivateRoute exact component={FollowingPage} path="/following" />
                    <PrivateRoute exact component={SavedPage} path="/saved" />
                    <PrivateRoute exact component={SettingsPage} path="/settings" />

                    <LoginRoute exact component={LoginPage} path="/login" />
                    <LoginRoute exact component={RegisterPage} path="/register" />
                </Switch>
            </Router>
        </div>
    )
}

export default Routes;