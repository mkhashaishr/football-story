import React, {useEffect} from "react";
import { Container, Stack, Row, Col } from "react-bootstrap";
import ClContent from "../Components/Apps/ClContent";
import ClHeader from "../Components/Apps/ClHeader";
import ClInfo from "../Components/Apps/ClInfo";
import Header from "../Components/Header";

const ClubPage = () => {

    useEffect(() => {
        document.title = "Football Story - Club"
    }, [])

    return (
        <>
            <Stack className="club-page">
                <Header />
                <Container>
                    <Row className="d-flex justify-content-between gy-3 content-page">
                        <Col lg="2" className="px-lg-1">
                            <ClInfo />
                        </Col>
                        <Col lg="10" className="px-lg-0">
                            <Stack>
                                <ClHeader />
                                <ClContent />
                            </Stack>
                        </Col>
                    </Row>
                </Container>
            </Stack>
        </>
    )
}

export default ClubPage;