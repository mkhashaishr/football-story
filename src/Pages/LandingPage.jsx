import React, {useEffect} from "react";
import { Stack } from "react-bootstrap";
import Header from '../Components/Header'
import Hero from "../Components/LandingPage/Hero";
import About from "../Components/LandingPage/About";
import Contact from "../Components/LandingPage/Contact";
import Footer from "../Components/LandingPage/Footer";
import Explore from "../Components/LandingPage/Explore";


const LandingPage = () => {

    useEffect(() => {
        document.title = "Football Story - Read the best story from Football World"
    }, [])

    return (
        <div className="landing-page">
            <Stack>
                <Header />
                <Hero />
                <About />
                <Explore />
                <Contact />
                <Footer />
            </Stack>
        </div>
    )
}

export default LandingPage;