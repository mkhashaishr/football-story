import React, {useEffect} from "react";
import { Container, Row, Col, Stack } from "react-bootstrap";
import Header from "../Components/Header";
import FContent from "../Components/Apps/FContent";
import SideMenu from "../Components/Apps/SideMenu";

const FollowingPage = () => {

    useEffect(() => {
        document.title = "Football Story - Following"
    }, [])

    return (
        <>
            <Stack>
                <Header />
                <Container>
                    <Row className="justify-content-center g-3 home-page" style={{marginTop: "6rem"}}>
                        <Col lg="3" className="d-flex justify-content-center px-0 gx-0">
                            <SideMenu />
                        </Col>
                        <Col lg="9" className="px-0 gx-0">
                            <Stack gap={3}>
                                <FContent />
                            </Stack>
                        </Col>
                    </Row>
                </Container>
            </Stack>
        </>
    )
}

export default FollowingPage;