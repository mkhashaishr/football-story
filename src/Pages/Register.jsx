import React, {useEffect, useState} from "react";
import { Card, Container, Row, Col, Button, Form } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { useHistory } from "react-router";
import axios from "axios";
import Cookies from "js-cookie"

const RegisterPage = () => {

    useEffect(() => {
        document.title = "Football Story - Register"
    }, [])

    let history = useHistory()
    const [input, setInput] = useState({ name: "", email: "", password: "" })

    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(input)
        axios.post(`https://backendexample.sanbersy.com/api/register`, {
            name: input.name,
            email: input.email,
            password: input.password
        }).then(
            () => {
                history.push('/login')
            }
        ).catch((err) => {
            alert(err)
        })
    }

    const handleChange = (event) => {
        let value = event.target.value
        let name = event.target.name

        setInput({ ...input, [name]: value })
    }

    return (
        <Row>
            <div class="position-absolute top-50 start-50 translate-middle login-register">
                <Card className="pt-5" style={{borderRadius: "10px"}}>
                    <Card.Body>
                        <div  className="text-center w-100">
                            <span className="title">FStory</span>
                        </div>
                        <Form onSubmit={handleSubmit}>
                            <Form.Group className="mb-3" controlId="formBasicEmail" onChange={handleChange} value={input.name}>
                                <Form.Label>Name</Form.Label>
                                <Form.Control type="text" name="name" placeholder="Enter name" required/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail" onChange={handleChange} value={input.email}>
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" name="email" placeholder="Enter email" required/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicPassword" onChange={handleChange} value={input.password}>
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" name="password" placeholder="Password" required/>
                            </Form.Group>
                            <Button variant="primary" type="submit" className="btnb text-white px-4 py-2">
                                Login
                            </Button>
                            <p className="mt-5">Already have an account? <NavLink to="/login">Login</NavLink></p>
                        </Form>
                    </Card.Body>
                </Card>
            </div>
        </Row>
    )
}

export default RegisterPage;