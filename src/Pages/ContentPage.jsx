import React, {useEffect} from "react";
import { Container,Stack, Row, Col } from "react-bootstrap";
import Header from "../Components/Header";
import CpContent from "../Components/Apps/CpContent"
import CpSidebarL from "../Components/Apps/CpSidebarL";
import CpSidebarR from "../Components/Apps/CpSidebarR";
import accounts from "../Assets/data/data";

const ContentPage = () => {

    let obj = accounts.find(e => e.id === parseInt('001'));

    useEffect(() => {
        document.title = `Football Story - ${obj.posts[1].title}`
    }, [])

    return (
        <>
            <Stack>
                <Header />
                <Container>
                    <Row className="justify-content-center g-3 content-page">
                        <Col lg="2" className="d-flex justify-content-center">
                            <CpSidebarL />
                        </Col>
                        <Col lg="8">
                            <CpContent />
                        </Col>
                        <Col lg="2" className="d-flex justify-content-center">
                            <CpSidebarR />
                        </Col>
                    </Row>
                </Container>
            </Stack>
        </>
    )
}

export default ContentPage;