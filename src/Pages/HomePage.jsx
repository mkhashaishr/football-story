import React, {useEffect} from "react";
import { Container, Row, Col, Stack } from "react-bootstrap";
import Header from "../Components/Header";
import HContent from "../Components/Apps/HContent";
import HExplore from "../Components/Apps/HExplore";
import SideMenu from "../Components/Apps/SideMenu";

const HomePage = () => {

    useEffect(() => {
        document.title = "Football Story - Home"
    }, [])

    return (
        <>
            <Stack>
                <Header />
                <Container>
                    <Row className="justify-content-center g-3 home-page" style={{marginTop: "6rem"}}>
                        <Col lg="3" className="d-flex justify-content-center px-0 gx-0">
                            <SideMenu />
                        </Col>
                        <Col lg="9" className="px-0 gx-0">
                            <Stack gap={3}>
                                <HExplore />
                                <HContent />
                            </Stack>
                        </Col>
                    </Row>
                </Container>
            </Stack>
        </>
    )
}

export default HomePage;